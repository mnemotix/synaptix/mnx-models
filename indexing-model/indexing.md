# Modèle d'indexation

Le modèle d'indexation Synaptix permet de décrire comment une classe RDF doit être indexée dans ElasticSearch.

## Ontologie

L'ontologie qui définit le modèle d'indexation peut être consultée [ici](https://gitlab.com/mnemotix/mnx-models/-/blob/master/indexing-model/indexing-model.owl).

![schema](https://gitlab.com/mnemotix/mnx-models/-/raw/master/indexing-model/mnx-indexing-model.png)

## Principe de fonctionnement

L'indexation dans Synaptix se déroule en deux étapes :
* La première étape consiste à requêter le graphe et récupérer les informations d'indexation afin construire les indexes ElasticSearch spécifiques à chaque classe dotée d'un `IndexingModel`.

* La deuxième étape consiste à utiliser les informations contenues dans le modèle d'indexation pour construire automatiquement des requêtes SPARQL afin de requêter les instances de chacune des classes à indexer et ainsi pouvoir alimenter l'index avec les données du graphe.


Pour chaque classe à indexer, il est nécessaire de déclarer un `IndexingModel` et de décrire chaque champ qui devra être indexé pour cette classe.
Chaque champ à indexer doit être décrit par un `IndexingField` qui précise le `im:dataPath` et le `im:fieldDatatype` du champ en question.

D'autres propriétés du modèle permettent de préciser la manière dont l'index ElasticSearch doit être construit et plups particulièrement les données de mapping. 

## Utilisation

Afin de pouvoir facilement mettre à jour les informations d'indexation nous conseillons de stocker ces triplets dans un graphe nommé facilement identifiable (ici `:IndexingDataNG`).

Le modèle consiste à créer une instance de `im:IndexingModel` et de la rattacher à une classe RDF grâce à la relation `im:indexingModelOf`.

Il est important de préciser les namespaces qui seront utilisés dans les `im:dataPath` afin que les requêtes SPARQL générées à partir du modèle disposent de toutes les informations requises pour s'exécuter sans erreur.
Pour cela, il faut utiliser la propriété `im:prefix` (voir exemple).

Ensuite, pour chaque instance de `im:IndexingModel` il faudra déclarer une liste de `im:IndexingField` pour chaque champ à indexer.

## Exemple
```turtle
@prefix im: <http://ns.mnemotix.com/ontologies/2019/1/indexing-model#> .
@prefix clr: <http://data.clairsienne.com/ontologies/2019/12/clr-patrimoine#> .
@prefix mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/> .
@prefix clrvoc: <http://data.clairsienne.com/data/2019/12/vocabulary/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix : <http://data.clairsienne.com/data/2019/12/clr-patrimoine/> .
@base <http://data.clairsienne.com/data/2019/12/clr-patrimoine> .

:IndexingDataNG {
    [
        a im:IndexingModel ;
        im:indexingModelOf clr:Lot ;
        im:prefix [
                      rdfs:label "clr" ;
                      im:value "http://data.clairsienne.com/ontologies/2019/12/clr-patrimoine#"
                  ],
                  [
                      rdfs:label "mnx" ;
                      im:value "http://ns.mnemotix.com/ontologies/2019/8/generic-model/"
                  ] ;
        im:field [
                     a im:IndexingField ;
                     rdfs:label "sha" ;
                     im:fieldDatatype xsd:decimal ;
                     im:dataPath "clr:SHA" ;
                     im:multivalued "false"^^xsd:boolean ;
                     im:analyzed "false"^^xsd:boolean ;
                     im:optional "true"^^xsd:boolean
                 ],
                 [
                     a im:IndexingField ;
                     rdfs:label "su" ;
                     im:fieldDatatype xsd:decimal ;
                     im:dataPath "clr:SU" ;
                     im:multivalued "false"^^xsd:boolean ;
                     im:analyzed "false"^^xsd:boolean ;
                     im:optional "true"^^xsd:boolean
                 ],
                 [
                     a im:IndexingField ;
                     rdfs:label "address" ;
                     im:fieldDatatype xsd:anyURI ;
                     im:dataPath "mnx:hasAddress" ;
                     im:multivalued "false"^^xsd:boolean ;
                     im:analyzed "false"^^xsd:boolean ;
                     im:optional "true"^^xsd:boolean ;
                     im:filterDeleted "true"^^xsd:boolean ;
                     im:subfield [
                                     a im:IndexingField ;
                                     rdfs:label "deptCode" ;
                                     im:fieldDatatype xsd:string ;
                                     im:dataPath "clr:deptCode" ;
                                     im:multivalued "false"^^xsd:boolean ;
                                     im:analyzed "false"^^xsd:boolean ;
                                     im:optional "true"^^xsd:boolean ;
                                 ],
                                 [
                                     a im:IndexingField ;
                                     rdfs:label "street1" ;
                                     im:fieldDatatype xsd:string ;
                                     im:dataPath "mnx:street1" ;
                                     im:multivalued "false"^^xsd:boolean ;
                                     im:analyzed "true"^^xsd:boolean ;
                                     im:optional "true"^^xsd:boolean ;
                                     im:analyzer "simple" ;
                                     im:ignore_above 500
                                 ],
                                 [
                                     a im:IndexingField ;
                                     rdfs:label "street2" ;
                                     im:fieldDatatype xsd:string ;
                                     im:dataPath "mnx:street2" ;
                                     im:multivalued "true"^^xsd:boolean ;
                                     im:analyzed "true"^^xsd:boolean ;
                                     im:optional "true"^^xsd:boolean ;
                                     im:analyzer "standard" ;
                                     im:ignore_above 500
                                 ]
                 ]
    ]
}
```